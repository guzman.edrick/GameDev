﻿var ant : GameObject;
var scoreText : GameObject;
var livesText : GameObject;
var walkingSpeed : double;
var livesNumber : int;
var scoreNumber : int;

function Start () {
	ant = GameObject.Find("Ant");
	scoreText = GameObject.Find("Score");
	livesText = GameObject.Find("Lives");

	//Initialize the values of walking speed
	walkingSpeed = 0.005;
	livesNumber = 12;
	scoreNumber = 0;

	//Initialize the GUI components
	livesText.GetComponent(UI.Text).text = "Units Left: " + livesNumber;
	scoreText.GetComponent(UI.Text).text = "Units you took: " + scoreNumber;

	//Place the ant in a random position on start of the game
	ant.transform.position.x = generateX();
	ant.transform.position.y = generateY();
}

function Update () {	

	if(ant.transform.position.y < -4.35 && livesNumber != 0){	
		
		livesNumber -= 3;
		livesText.GetComponent(UI.Text).text = "Units Left: " + livesNumber;
		generateCoordinates();

	}else if(ant.transform.position.y < -4.35 && livesNumber == 0){
		Destroy(GameObject.Find("Ant"));
		gameOver();

	}else if(livesNumber == 0){
		Destroy(GameObject.Find("Ant"));
		gameOver();

	}
	else if(scoreNumber >= 100){
		Destroy(GameObject.Find("Ant"));
		gameWin();

	}else{

		ant.transform.position.y -= walkingSpeed;
	}
}

function gameOver(){	
	Application.LoadLevel("GameOver");
}

function gameWin(){	
	Application.LoadLevel("GameWin");
}


//Generates random x
function generateX(){
	var x = Random.Range(-4.54,4.54);
	return x;
}

//Generates random y
function generateY(){
	var y = Random.Range(1,4.7);
	return y;
}

//Move the "Ant" to the new coordiantess
function generateCoordinates(){
	//You clicked it!
	scoreNumber += 3;

	//Update the score display
	scoreText.GetComponent(UI.Text).text = "Units you took: " + scoreNumber;
	ant.transform.position.x = generateX();
	ant.transform.position.y = generateY();
}

//If tapped or clicked
function OnMouseDown(){
	//Place the ant at another point
	generateCoordinates();

	//Increase the walking speed by 0.
	walkingSpeed += 0.002;
}