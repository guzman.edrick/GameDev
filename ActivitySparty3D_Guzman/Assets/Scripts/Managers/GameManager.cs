﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

	// Use this for initialization

	public List<Coin> coins;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (coins.Count == 0) {
			SceneManager.LoadScene ("GameWin");
		}
	}
}
