﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagCapturer : MonoBehaviour {

	private GameObject flag;

	public bool HasFlag {
		get {return flag != null;}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (HasFlag) {
			flag.transform.position = new Vector3(transform.position.x, flag.transform.position.y, transform.position.z);
		}
	}

	public void DropFlag() {
		if (flag == null) return;
		flag = null;
	}

	public void DropFlagToOriginal() {
		if (flag == null) return;
		flag.GetComponent<Flag>().GoToOriginalPos();
		flag = null;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Flag") {
			flag = other.gameObject;
		}

		if (other.tag == "Base" && HasFlag) {
			DropFlag();
			MyGameManager.Instance.CapturedFlag();
		}
	}
}
