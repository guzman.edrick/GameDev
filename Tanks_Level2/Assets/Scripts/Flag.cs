﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour {

	private Vector3 originalPos;

	public Vector3 OriginalPos {
		get {return originalPos;}
	}

	// Use this for initialization
	void Start () {
		originalPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GoToOriginalPos() {
		transform.position = originalPos;
	}
}
