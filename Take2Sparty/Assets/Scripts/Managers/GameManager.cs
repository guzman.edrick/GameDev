﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

	public Camera mainCamera;
	public List<GameObject> coins;
	public GameObject apple;

	public string nextLevel;
	public float delayBeforeNextLevel = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool CheckApple(GameObject apple) {
		if (apple == null) return false;
		if (this.apple == apple) {
			NextLevel(nextLevel,delayBeforeNextLevel);

			return true;
		}

		return false;
	}

	public void RemoveCoin(GameObject coin) {
		coins.Remove(coin);
	}

	public void NextLevel(string nextLevel) {
		StartCoroutine(IENextLevel(nextLevel,0));
	}

	public void NextLevel(string nextLevel, float waitFor) {
		StartCoroutine(IENextLevel(nextLevel,waitFor));
	}

	 IEnumerator IENextLevel(string nextLevel, float waitFor) {
		yield return new WaitForSeconds(waitFor);

		SceneManager.LoadScene(nextLevel);
	}
}
